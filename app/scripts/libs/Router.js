/**
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export default function RouterInstance () {

  if (typeof window.RouterInstance_ !== 'undefined')
    return window.RouterInstance_;

  window.RouterInstance_ = new Router();

  return window.RouterInstance_;
}

class Router {

  constructor () {
    this.routes = [];
    this.currentAction = null;

    window.addEventListener('popstate', (e) => {
      e.preventDefault();
      this.manageState();
    });

    this.manageState();
  }

  on (path, callbackIn, callbackOut, callbackUpdate) {
      var route = {
        matcher: new RegExp(path.replace(/:[^\s/]+/g, '([\\w-]+)')),
        in: callbackIn,
        out: callbackOut,
        update: callbackUpdate
      };
      this.routes.push(route);

      var newCheck = window.location.pathname.match(route.matcher);
      if (newCheck) callbackIn.apply(this, newCheck.slice(1))
  }

  back(){
    history.back();
    this.manageState();
  }

  go (path) {

    // Only process real changes.
    if (path === window.location.pathname)
      return;

    history.pushState(undefined, "", path);
    this.manageState();
  }

  manageState() {
    var path = window.location.pathname;
    this.routes.forEach(route => {
      var newCheck = path.match(route.matcher);
      var oldCheck = this.currentAction ? this.currentAction.match(route.matcher) : null;
      if (!newCheck && !oldCheck) return;

      if (!newCheck && oldCheck && route.out) 
        return route.out();
      else if (newCheck && !oldCheck && route.in) 
        return route.in.apply(this, newCheck.slice(1));
      else if (route.update) 
        return route.update.apply(this, newCheck.slice(1));
    });
    this.currentAction = path;
  }
}
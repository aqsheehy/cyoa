import FirebaseInstance from './Firebase';

export default function FileUploadInstance () {

  if (typeof window.FileUploadInstance_ !== 'undefined')
    return window.FileUploadInstance_;

  window.FileUploadInstance_ = new FileUpload();

  return window.FileUploadInstance_;
  
}

class FileUpload {

    constructor(){
        this.firebase = FirebaseInstance();
    }

    async upload(element){
        var file = element.files[0];
        if (file == null) return Promise.resolve(null);
        var storage = await this.firebase.storage;
        var snapshot = await storage.ref(`files/${randId()}`).put(file);
        return snapshot.downloadURL;
    }

}

function randId() {
     return Math.random().toString(36).substr(2, 10);
}